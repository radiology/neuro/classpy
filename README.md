# ClassPy - A python toolbox for classification

Note: For use on a cluster environment, multithreading should be disabled. Otherwise pandas will use a huge amount of memory. 
Can be solved by adding 'export OMP_NUM_THREADS=1' to .bashrc
