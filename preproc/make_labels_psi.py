'''
Write CSV file with MCI converters
'''

import classpy.parelsnoer_input as pi
import pandas as pd

PSI_filenames = 'C://Users//Esther Bron//Documents//Project//Parelsnoer//Showcase//PSI_filenames.txt'
df = pd.read_csv(PSI_filenames, header = None)
df.rename(columns = {list(df)[0]: 'filename'}, inplace = True)
df['id_number'] = [i[-5:] for i in df['filename']]

str_data_element = 'C://Users//Esther Bron//Documents//Project//Parelsnoer//Showcase//MAIN_RESTRUCTURED_NEW_with_queries_match_with_XNAT_showcase_556_original_columns.sav'
Data = pd.read_csv(str_data_element)
Data_temp = pi.prepare_data(Data.copy(), minimum_followup_months=6, write_csv=0)
Data_temp['id_number'] = [i[-5:] for i in Data_temp['PTID']]

Data_csv= Data_temp.merge(df, on="id_number", how = 'inner')

removeColumns = ['Age', 'Sex', 'PTID', 'id_number']
Data_csv = Data_csv.drop(removeColumns, axis=1)
Data_csv = Data_csv.rename(columns={'filename': 'PTID'})

cols = Data_csv.columns.tolist()
cols_new = [i for sublist in [[cols[-1]], cols[:-1]] for i in sublist]
Data_csv = Data_csv[cols_new]

Data_csv.to_csv('C://Users//Esther Bron//Documents//Project//Parelsnoer//Showcase//labels_psi.csv', index=False)


removeColumns = ['Diagnosis', 'MCI converter (6)']
Data_csv = Data_csv.drop(removeColumns, axis=1)
Data_csv = Data_csv.rename(columns={'Diagnosis MCI': 'Diagnosis'})
Data_csv.to_csv('C://Users//Esther Bron//Documents//Project//Parelsnoer//Showcase//labels_mci_psi.csv', index=False)