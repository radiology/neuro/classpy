'''
Write CSV file with MCI converters
'''

import classpy.tadpole_input as ti
import pandas as pd


str_data_element = 'C://Users//Esther Bron//Documents//Project//ADNI//TADPOLE_D1_D2_demographics.csv'
Data = pd.read_csv(str_data_element)
Data_temp = ti.prepare_data(Data.copy(), maximum_followup_months=36, write_csv=0)

Data_bl = Data_temp[Data_temp['ID'].str.contains("_bl")]
removeColumns = ['ID', 'Age', 'Sex', 'ICV']
Data_csv = Data_bl.drop(removeColumns, axis=1)

Data_csv.to_csv('C://Users//Esther//Documents//Project//ADNI//adni_labels.csv')