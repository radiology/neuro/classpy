'''
Write Parelsnoer datafile with correct mri ids matching to the actual files (underscore or not!)
'''

import pandas as pd

PSI_filenames = 'C://Users//Esther Bron//Documents//Project//Parelsnoer//Showcase//PSI_filenames.txt'
df = pd.read_csv(PSI_filenames, header = None)
df.rename(columns = {list(df)[0]: 'filename'}, inplace = True)
df['id_number'] = [i[-5:] for i in df['filename']]

str_data_element = 'C://Users//Esther Bron//Documents//Project//Parelsnoer//Showcase//MAIN_RESTRUCTURED_NEW_with_queries_match_with_XNAT_showcase_556_original_columns.sav'
Data = pd.read_spss(str_data_element)
Data['id_number'] = [i[-5:] for i in Data['MRI_ID_XNAT']]

Data_csv= Data.merge(df, on="id_number", how = 'inner')

removeColumns = ['MRI_ID_XNAT', 'id_number']
Data_csv = Data_csv.drop(removeColumns, axis=1)
Data_csv = Data_csv.rename(columns={'filename': 'MRI_ID_XNAT'})

Data_csv.to_csv('C://Users//Esther Bron//Documents//Project//Parelsnoer//Showcase//MAIN_RESTRUCTURED_NEW_with_queries_match_with_XNAT_showcase_556_original_columns_correct_ids.csv', index=False)