import pandas as pd
import numpy as np
import statsmodels.formula.api as sm
import datetime
import time
import classpy.tadpole_input as ti
import classpy.parelsnoer_input as pi


def pdReadData(str_data, flag_JointFit=False, Labels=None):

    maximum_followup_months=36
    if Labels is None:
        Labels = ['CN', 'MCI', 'AD']
    
    if type(str_data) is str:
        Data = pd.read_csv(str_data)
        
    elif type(str_data) is list and str_data:
        Data = pd.read_csv(str_data[0])

        if 'TADPOLE' in str_data[0]:                
            Data = ti.prepare_data(Data, maximum_followup_months=maximum_followup_months)
        if 'showcase' in str_data[0]:                
            Data = pi.prepare_data(Data, minimum_followup_months=6)  
        
        for str_data_element in str_data[1:]:
            if str_data_element.endswith('.csv'):
                Data_temp = pd.read_csv(str_data_element)
            elif str_data_element.endswith('.sav'):
                Data_temp = pd.read_spss(str_data_element)                

            if 'TADPOLE' in str_data_element:                
                Data_temp = ti.prepare_data(Data_temp, maximum_followup_months=maximum_followup_months)
            if 'showcase' in str_data_element:                
                Data_temp = pi.prepare_data(Data_temp, minimum_followup_months=6)    

            #Data = Data.set_index('PTID').join(Data_temp.set_index('PTID'))
            Data = pd.concat([Data, Data_temp], sort=True)
    elif len(str_data) == 0:
        Data = []
        UniqueSubjIDs = []
    
        return Data, UniqueSubjIDs
    else:
        Data = str_data
    UniqueSubjIDs = pd.Series.unique(Data['PTID'])
    labels = np.zeros(len(Data['Diagnosis']), dtype=int)

    Data = Data.rename(columns=lambda x: x.replace(' ','_').replace('(','').replace(')',''))

    # HERe something wrong in ADNI?
    for i in range(len(Labels)):
        print(i)
        print(Labels[i])
        if Labels[i]=='MCIc':
            if 'MCI_converter_' + str(maximum_followup_months) in Data.keys():
                idx_label = (Data['Diagnosis'] == 'MCI') & (Data['MCI_converter_' + str(maximum_followup_months)])
            else:
                idx_label = Data['Diagnosis'] == 'MCIc'
        elif Labels[i]=='MCInc': 
            if 'MCI_converter_' + str(maximum_followup_months) in Data.keys():
                idx_label = (Data['Diagnosis'] == 'MCI') & (Data['MCI_converter_' + str(maximum_followup_months)] == 0)
            else:
                idx_label = Data['Diagnosis'] == 'MCI'
        else:
            idx_label = Data['Diagnosis'] == Labels[i]
        
        idx_label = np.where(idx_label.values)
            
        idx_label = idx_label[0]
        print(idx_label)
        labels[idx_label] = i + 1
        print(np.array(labels))
    print(labels)
    idx_selectedsubjects = np.logical_not(labels == 0)
    labels_selected = labels[np.logical_not(labels == 0)]
    Data = Data[idx_selectedsubjects]
    Data = Data.drop('Diagnosis', axis=1)
    Data = Data.assign(Diagnosis=pd.Series(labels_selected, Data.index))

    return Data, UniqueSubjIDs


def CorrectConfounders(DataTrain,
                       DataTest,
                       Factors=None,
                       flag_correct=1,
                       Groups=None):

    if Factors is None:
        Factors = ['Age', 'Sex', 'ICV']
    if Groups is None:
        Groups = []

    print('Correcting for confounders')
    flag_test = 1
    droplist = ['PTID', 'Diagnosis', 'EXAMDATE']
    GroupValues = []
    if flag_correct == 0 or len(Factors) == 0:
        DataTrain = DataTrain.drop(Factors, axis=1)
        DataBiomarkers = DataTrain.copy()
        H = list(DataBiomarkers)
        for j in droplist:
            if any(j in f for f in H):
                DataBiomarkers = DataBiomarkers.drop(j, axis=1)
        BiomarkersList = list(DataBiomarkers)
        if len(DataTest) > 0:
            DataTest = DataTest.drop(Factors, axis=1)
    else:
        # Change categorical value to numerical value
        if len(DataTest) == 0:
            flag_test = 0
            DataTest = DataTrain.copy()

        if any('Sex' in f for f in Factors):
            count = -1
            for Data in [DataTrain, DataTest]:
                count = count+1

                sex = np.zeros(len(Data['Diagnosis']), dtype=int)

                idx_male = Data['Sex'] == 'Male'
                idx_male = np.where(idx_male)
                idx_male = idx_male[0]

                idx_female = Data['Sex'] == 'Female'
                idx_female = np.where(idx_female)
                idx_female = idx_female[0]

                sex[idx_male] = 1
                sex[idx_female] = 0

                Data = Data.drop('Sex', axis=1)
                Data = Data.assign(Sex=pd.Series(sex, Data.index))
                if count == 0:
                    DataTrain = Data.copy()
                else:
                    DataTest = Data.copy()

        # Separate the list of biomarkers from confounders and meta data
        count = -1
        for Data in [DataTrain, DataTest]:
            count = count+1
            idx = Data['Diagnosis']==1
            DataBiomarkers = Data
            DataBiomarkers = DataBiomarkers.drop(Factors, axis=1)

            H = list(DataBiomarkers)
            for j in droplist:
                if any(j in f for f in H):
                    DataBiomarkers = DataBiomarkers.drop(j, axis=1)
        
            for j in Groups:
                GroupValues.append([])
                if any(j in f for f in H):
                    GroupValues[-1].append(DataBiomarkers[j])
                    DataBiomarkers = DataBiomarkers.drop(j, axis=1)
        
            BiomarkersList = list(DataBiomarkers)
            BiomarkersListnew = []
            for i in range(len(BiomarkersList)):
                BiomarkersListnew.append(BiomarkersList[i].replace(' ', '_'))
                BiomarkersListnew[i] = BiomarkersListnew[i].replace('-', '_')

            for i in range(len(BiomarkersList)):
                Data = Data.rename(columns={BiomarkersList[i]: BiomarkersListnew[i]})

            # Construct the formula for regression.
            # Compute the mean value of the confounding factors for correction
            if count == 0:  # Do it only for training set
                str_confounders = ''
                mean_confval = np.zeros(len(Factors))
                for j in range(len(Factors)):
                    str_confounders = str_confounders + '+' + Factors[j]
                    mean_confval[j] = np.nanmean(Data[Factors[j]].values)
                str_confounders = str_confounders[1:]

                # Multiple linear regression
                betalist = []
                for i in range(len(BiomarkersList)):
                    str_formula = BiomarkersListnew[i] + '~' + str_confounders
                    result = sm.ols(formula=str_formula, data=Data[idx]).fit()
                    betalist.append(result.params)

            # Correction for the confounding factors
            Deviation = (Data[Factors] - mean_confval)
            Deviation[np.isnan(Deviation)] = 0
            for i in range(len(BiomarkersList)):
                betai = betalist[i].values
                betai_slopes = betai[1:]
                CorrectionFactor = np.dot(Deviation.values, betai_slopes)
                Data[BiomarkersListnew[i]] = Data[BiomarkersListnew[i]] - CorrectionFactor
            Data = Data.drop(Factors, axis=1)
            Data = Data.drop(Groups, axis=1)

            for i in range(len(BiomarkersList)):
                Data = Data.rename(columns={BiomarkersListnew[i]:BiomarkersList[i]})
            if count == 0:
                DataTrain = Data.copy()
            else:
                DataTest = Data.copy()

    if flag_test == 0:
        DataTest = []
        if len(Groups) == 0:
            GroupValues = []
        else:
            GroupValues = GroupValues.pop()

    print(list(DataTrain.columns)[:10])
    return DataTrain, DataTest, BiomarkersList, GroupValues


def pd2mat(pdData, BiomarkersList, flag_JointFit):

    # Convert arrays from pandas dataframe format to the matrices
    # (which are used in DEBM algorithms)
    num_events = len(BiomarkersList)
    if flag_JointFit == 0:
        num_feats = 1
    num_subjects = pdData.shape[0]
    matData = np.zeros((num_subjects, num_events, num_feats))
    for i in range(num_events):
        matData[:, i, 0] = pdData[BiomarkersList[i]].values

    return matData


def ExamDate_str2num(ExamDateSeries):

    timestamp = np.zeros(len(ExamDateSeries))
    for i in range(len(ExamDateSeries)):
        stre = ExamDateSeries.values[i]
        if len(stre) > 5:
            timestamp[i] = time.mktime(datetime.datetime.strptime(stre, "%Y-%m-%d").timetuple())
        else:
            timestamp[i] = np.nan
    TimestampSeries = pd.Series(timestamp)

    return TimestampSeries
