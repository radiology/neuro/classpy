from configparser import SafeConfigParser, NoOptionError
import os
import datetime

def read_config(config_file='./resources/config.ini'):
    p={}
    
    # Defaults
    # Main
    p['DataIn'] = './resources/Data_7.csv'
    p['DataTest'] = []
    p['Labels'] = ['CN', 'MCI', 'AD']  
    p['Factors'] = []
    p['Biomarkers_exclude'] = []

    # Output
    p['ResultsFile'] = './resources/classpy_results.csv'
    p['SaveResults'] = 1
    p['TempDirectory'] = './resources'
    p['InputTempFile'] = None  # Continue from previous temp file

    # Crossvalidation   
    p['Type'] = 'RepeatedStratifiedKFold'
    p['TestClass'] = []
    p['Iterations'] = 1 
    p['N_splits'] = 2
    p['test_size'] = 0.1
    p['Kernel'] = 'precomputed'
    
    # Read config file
    config = SafeConfigParser()
    config.read(config_file)
    
    # Read variables into dict P 
    # MAIN   
    try: 
        p['DataDescription'] = config.get('main','DataDescription')
    except NoOptionError:
        pass

    try: 
        p['DataIn'] = config.get('main','DataIn').split(',')
    except NoOptionError:
        pass
    
    try: 
        p['DataTest'] = config.get('main','DataTest').split(',')
    except NoOptionError:
        pass
    
    try: 
        p['Labels'] = config.get('main','Labels').split(',')
    except NoOptionError:
        pass
    
    try: 
        p['Factors'] = config.get('main','Factors').split(',')
    except NoOptionError:
        pass
    
    try: 
        p['Biomarkers_exclude'] = config.get('main','Biomarkers_exclude').split(',')
    except NoOptionError:
        pass
    
    try: 
        p['UseSavedModel'] = config.get('main','UseSavedModel')
    except NoOptionError:
        pass    
    
    
    # Output
    try:  
        p['ResultsFile'] = config.get('output','ResultsFile')
    except NoOptionError:
        pass
    
    try: 
        p['SaveResults'] = config.get('output','SaveResults')
    except NoOptionError:
        pass
    
    try:  
        p['TempDirectory'] = config.get('output','TempDirectory')
    except NoOptionError:
        pass
    
    try:  
        p['InputTempFile'] = config.get('output','InputTempFile')
    except NoOptionError:
        pass
    
    ## Crossvalidation
    try: 
        p['Type'] = config.get('crossvalidation','Type')
    except NoOptionError:
        pass

    try: 
        p['TestClass'] = config.get('crossvalidation','TestClass').split(',')
    except NoOptionError:
        pass
    
    try: 
        p['Iterations'] = int(config.get('crossvalidation','Iterations'))
    except NoOptionError:
        pass
    
    try:
        p['N_splits'] = int(config.get('crossvalidation','N_splits'))
    except NoOptionError:
        pass
 
    try:
        p['test_size'] = float(config.get('crossvalidation','test_size'))
    except NoOptionError:
        pass    
    
    try:
        p['Kernel'] = config.get('crossvalidation','Kernel')
    except NoOptionError:
        pass
        
    p['DataInDirectory'] = [os.path.dirname(i) for i in p['DataIn']]
    p['DataTestDirectory'] = [os.path.dirname(i) for i in p['DataTest']]
    p['ResultsFolder'] = os.path.join(p['TempDirectory'], p['DataDescription'] + '-' + str(datetime.datetime.now()).split('.')[0].replace(' ', '-').replace(':', '-'))
    if p['UseSavedModel'] == '0':
        p['UseSavedModel'] = ''
    
    return p

    