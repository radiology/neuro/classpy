import os
import pandas as pd 

def save_results_as_csv(p, ResultsFile):    
    if os.path.exists(ResultsFile):
        dfResults = pd.read_csv(ResultsFile)
    else:    
        dfResults = pd.DataFrame()
        
    dfResults = dfResults.append(pd.DataFrame([p.values()],
                columns=p.keys()),sort=False)
    dfResults.to_csv(ResultsFile,index=False)
    print('Results saved to: ', ResultsFile)
