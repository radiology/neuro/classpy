import numpy
from sklearn import metrics

def pairwise_auc(y_true, y_score, class_i, class_j):
    # Filter out the probabilities for class_i and class_j
    y_score = [est[class_i] for ref, est in zip(y_true, y_score) if ref in (class_i, class_j)]
    y_true = [ref for ref in y_true if ref in (class_i, class_j)]

    # Sort the y_true by the estimated probabilities
    sorted_y_true = [y for x, y in sorted(zip(y_score, y_true), key=lambda p: p[0])]

    # Calculated the sum of ranks for class_i
    sum_rank = 0
    for index, element in enumerate(sorted_y_true):
        if element == class_i:
            sum_rank += index + 1
    sum_rank = float(sum_rank)

    # Get the counts for class_i and class_j
    n_class_i = float(y_true.count(class_i))
    n_class_j = float(y_true.count(class_j))

    # If a class in empty, AUC is 0.0
    if n_class_i == 0 or n_class_j == 0:
        return 0.0

    # Calculate the pairwise AUC
    return (sum_rank - (0.5 * n_class_i * (n_class_i + 1))) / (n_class_i * n_class_j)


def multi_class_auc(y_true, y_score):
    classes = numpy.unique(y_true)

    if any(t == 0.0 for t in numpy.sum(y_score, axis=1)):
        raise ValueError('No AUC is calculated, output probabilities are missing')

    pairwise_auc_list = [0.5 * (pairwise_auc(y_true, y_score, i, j) +
                                pairwise_auc(y_true, y_score, j, i)) for i in classes for j in classes if i < j]

    c = len(classes)
    return (2.0 * sum(pairwise_auc_list)) / (c * (c - 1))

def multi_class_auc_score(y_true, y_score):
    return metrics.make_scorer(multi_class_auc, needs_proba=True)

