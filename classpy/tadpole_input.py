import numpy as np
import pandas as pd


def prepare_data(Dtadpole, maximum_followup_months=36, write_csv=0):

    # Set ID
    Dtadpole['RID'] = Dtadpole['PTID'] + '_' + Dtadpole['VISCODE']
    Dtadpole = Dtadpole.rename(columns={'RID': 'ID'})

    # Set variable names confounders
    Dtadpole = Dtadpole.rename(columns={'PTGENDER': 'Sex'})
    Dtadpole = Dtadpole.rename(columns={' Intracranial volume (mL)': 'ICV'})
    Dtadpole['AGE'] += Dtadpole['Month_bl'] / 12.
    Dtadpole = Dtadpole.rename(columns={'AGE': 'Age'})  

    # Clean-up file
    removeColumns = ['VISCODE', 'SITE', 'D1', 'D2', 'COLPROT', 'ORIGPROT', 'EXAMDATE', 'DX_bl', 'PTEDUCAT', 'PTETHCAT', 'PTRACCAT', 'PTMARRY', 'APOE4', 'FDG', 'PIB', 'AV45', 'CDRSB', 'ADAS11', 'ADAS13', 'MMSE', 'RAVLT_immediate', 'RAVLT_learning', 'RAVLT_forgetting', 'RAVLT_perc_forgetting', 'FAQ', 'MOCA', 'EcogPtMem', 'EcogPtLang', 'EcogPtVisspat', 'EcogPtPlan', 'EcogPtOrgan', 'EcogPtDivatt', 'EcogPtTotal', 'EcogSPMem', 'EcogSPLang', 'EcogSPVisspat', 'EcogSPPlan', 'EcogSPOrgan', 'EcogSPDivatt', 'EcogSPTotal', 'FLDSTRENG', 'FSVERSION', 'Ventricles', 'Hippocampus', 'WholeBrain', 'Entorhinal', 'Fusiform', 'MidTemp', 'DX', 'EXAMDATE_bl', 'CDRSB_bl', 'ADAS11_bl', 'ADAS13_bl', 'MMSE_bl', 'RAVLT_immediate_bl', 'RAVLT_learning_bl', 'RAVLT_forgetting_bl', 'RAVLT_perc_forgetting_bl', 'FAQ_bl', 'FLDSTRENG_bl', 'FSVERSION_bl', 'Ventricles_bl', 'Hippocampus_bl', 'WholeBrain_bl', 'Entorhinal_bl', 'Fusiform_bl', 'MidTemp_bl', 'ICV_bl', 'MOCA_bl', 'EcogPtMem_bl', 'EcogPtLang_bl', 'EcogPtVisspat_bl', 'EcogPtPlan_bl', 'EcogPtOrgan_bl', 'EcogPtDivatt_bl', 'EcogPtTotal_bl', 'EcogSPMem_bl', 'EcogSPLang_bl', 'EcogSPVisspat_bl', 'EcogSPPlan_bl', 'EcogSPOrgan_bl', 'EcogSPDivatt_bl', 'EcogSPTotal_bl', 'FDG_bl', 'PIB_bl', 'AV45_bl', 'Years_bl', 'Month', 'M']
    Dtadpole = Dtadpole.drop(removeColumns,axis=1)
    h = list(Dtadpole)

    urid = np.unique(Dtadpole['PTID'].values)
    print(urid)

    # Set Basline diagnosis
    # 1=Stable:NL to NL, 2=Stable:MCI to MCI, 3=Stable:AD to AD, 4=Conv:NL to MCI, 5=Conv:MCI to AD, 6=Conv:NL to AD, 7=Rev:MCI to NL, 8=Rev:AD to MCI, 9=Rev:AD to NL, -1=Not available
    idx_cn = Dtadpole['DXCHANGE'] == 1
    Dtadpole.loc[idx_cn, 'Diagnosis'] = 'CN'
    idx_mci = Dtadpole['DXCHANGE'] == 2
    Dtadpole.loc[idx_mci, 'Diagnosis'] = 'MCI'
    idx_ad = Dtadpole['DXCHANGE'] == 3
    Dtadpole.loc[idx_ad, 'Diagnosis'] = 'AD'

    idx_mci = Dtadpole['DXCHANGE'] == 4
    Dtadpole.loc[idx_mci, 'Diagnosis'] = 'MCI'
    idx_ad = Dtadpole['DXCHANGE'] == 5
    Dtadpole.loc[idx_ad, 'Diagnosis'] = 'AD'
    idx_ad = Dtadpole['DXCHANGE'] == 6
    Dtadpole.loc[idx_ad, 'Diagnosis'] = 'AD'
    idx_cn = Dtadpole['DXCHANGE'] == 7
    Dtadpole.loc[idx_cn, 'Diagnosis'] = 'CN'
    idx_mci = Dtadpole['DXCHANGE'] == 8
    Dtadpole.loc[idx_mci, 'Diagnosis'] = 'MCI'
    idx_cn = Dtadpole['DXCHANGE'] == 9
    Dtadpole.loc[idx_cn, 'Diagnosis'] = 'CN'

    # Sort the dataframe based on age for each subject
    Dtadpole_sorted = pd.DataFrame(columns=h)
    for i in range(len(urid)):
        agei = Dtadpole.loc[Dtadpole['PTID']==urid[i], 'Age']
        idx_sortedi = np.argsort(agei)
        D1 = Dtadpole.loc[idx_sortedi.index[idx_sortedi]]
        ld = [Dtadpole_sorted, D1]
        Dtadpole_sorted = pd.concat(ld, sort=False)
        
    Dtadpole = Dtadpole_sorted.copy()
    del Dtadpole_sorted
        
    # Find MCI converters within maximum_followup_months
    for i in range(len(urid)):
        months_list = list(Dtadpole.loc[Dtadpole['PTID'] == urid[i], 'Month_bl'])
        # Check if timepoints within maximum_followup_months range
        timepoint_valid = [t - months_list[0] < maximum_followup_months for t in months_list]
        # Keep only timepoint with a diagnostic label
        diagnosis_list = list(Dtadpole.loc[Dtadpole['PTID'] == urid[i], 'DXCHANGE'])
        valid_diagnosis_list = [d for j, d in enumerate(diagnosis_list) if timepoint_valid[j] and not np.isnan(d)]
        # Find MCI converters
        try:
            if valid_diagnosis_list[-1] != valid_diagnosis_list[0]:
                # DXCHANGE 5 means converted from MCI to AD
                if 5 in valid_diagnosis_list[1:]: 
                    if write_csv:                    
                        Dtadpole.loc[Dtadpole['PTID'] == urid[i], 'MCI converter (' + str(maximum_followup_months) + ')'] = 1
                    else:
                        if valid_diagnosis_list[0] in [2,4,8]:
                            Dtadpole.loc[Dtadpole['PTID'] == urid[i], 'Diagnosis'] = 'MCIc'
        except IndexError:
            pass

    removeColumns = ['Month_bl', 'DXCHANGE']
    Dtadpole = Dtadpole.drop(removeColumns, axis=1)

    return Dtadpole