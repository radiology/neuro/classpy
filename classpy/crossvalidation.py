from sklearn import preprocessing
from sklearn.model_selection import GridSearchCV
from sklearn.svm import SVC
from sklearn.model_selection import RepeatedStratifiedKFold, StratifiedShuffleSplit, PredefinedSplit
from sklearn.metrics import roc_auc_score, accuracy_score, confusion_matrix
from sklearn.metrics import make_scorer
import numpy as np
import pandas as pd
from classpy.multi_class_auc import multi_class_auc
import classpy.compute_CI as compute_CI
import pickle
import os
import time
import sys

def crossvalidation(Data_complete,
                    Diagnosis_complete,
                    SubjectID=[],
                    Labels=None,
                    Type='RepeatedStratifiedKFold',
                    TestClass=None,
                    iterations=3,
                    n_splits=5,
                    test_size=0.1,
                    kernel='linear',
                    random_state=0,
                    SaveTempFile=True,
                    TempFileLocation='.',
                    InputTempFile=None,
                    UseSavedModel='',
                    ResultsFolder=None):

    print(f"Starting {Type} crossvalidation...")
    print('Dataset: ' + str(Data_complete.shape[1]) + ' features and ' + str(Data_complete.shape[0]) + ' samples.' )
    if Labels is None:
        Labels = ['CN', 'MCI', 'AD']
    if TestClass is None:
        TestClass = []
        
    t = time.time()
    
    if Type == "PredefinedSplit":
        # Define test set (e.g. MCI class)
        test_index = [Labels.index(i) + 1 for i in TestClass]
        test_fold = [1 if i in test_index else -1 for i in Diagnosis_complete]
        
        # Reset labels for MCI class 
        Labels_dummy = Labels + [i if i not in Labels else '' for i in ['MCInc', 'MCI', 'CN_PSI', 'MCIc', 'AD_PSI']]
        
        print(Diagnosis_complete)
        print(Labels_dummy)
        Diagnosis_complete = pd.Series(Diagnosis_complete).map({Labels_dummy.index('CN') + 1: Labels.index('CN') + 1,
                                                                Labels_dummy.index('MCInc') + 1: Labels.index('CN') + 1, 
                                                                Labels_dummy.index('MCI') + 1: Labels.index('CN') + 1, 
                                                                Labels_dummy.index('CN_PSI') + 1: Labels.index('CN') + 1, 
                                                                Labels_dummy.index('AD') + 1: Labels.index('AD') + 1,
                                                                Labels_dummy.index('MCIc') + 1: Labels.index('AD') + 1, 
                                                                Labels_dummy.index('AD_PSI') + 1: Labels.index('AD') + 1}).to_numpy()
        
#        for tc in TestClass:
#            if tc in ['MCInc', 'MCI', 'CN_PSI']:
#                Diagnosis_complete = [ Labels.index('CN') + 1 if i==Labels.index(tc) + 1 else i for i in Diagnosis_complete]
#            if tc in ['MCIc', 'AD_PSI']:
#                Diagnosis_complete = [ Labels.index('AD') + 1 if i==Labels.index(tc) + 1 else i for i in Diagnosis_complete]
        print(Diagnosis_complete)
        
    # Standarization of data and labels
    Data_standardized = preprocessing.scale(Data_complete)
    nFeatures = Data_standardized.shape[1]

    lb = preprocessing.LabelEncoder()
    Diagnosis_standardized = np.squeeze(lb.fit_transform(Diagnosis_complete))
    
    print(Diagnosis_standardized)

    del Data_complete
    del Diagnosis_complete
        
    # Compute linear kernel if needed
    if kernel == 'precomputed':
        Data_standardized = np.dot(Data_standardized, Data_standardized.T)  # linear kernel
         
    if InputTempFile:
        InputTempFile = os.path.join(TempFileLocation, InputTempFile)
        TempFile=InputTempFile
        with open(TempFile, 'rb') as TF:
            a = pickle.load(TF)
            count_previous = a[0]
            for (i1, i2) in zip([Data_standardized, Diagnosis_standardized, Labels, iterations, n_splits, kernel], a[2:7]):
                if not np.all(i1 == i2):
                    print('Temporary file cannot be used.')
                    print(i1, ' is not corresponding to ', i2)
                    print(len(i1))
                    print(len(i2))
            clfs = a[-7]
            conf = a[-6]
            best_c_values = a[-5]
            accs = a[-4]
            aucs = a[-3]
            train_indices = a[-2]
            test_indices = a[-1]
    else:
        TempFile = os.path.join(ResultsFolder, 'temp_results.pickle')

        train_indices = []
        test_indices = []

        clfs = []
        best_c_values = []
        accs = []
        aucs = []
        count_previous=0
        
    model_filename = os.path.join(ResultsFolder, 'model.pickle')
    
    
    # Crossvalidation (kfold, stratified over classes, iterated multiple times)
    #random_state = 0
    if Type == "RepeatedStratifiedKFold":
        kf = RepeatedStratifiedKFold(n_splits=n_splits,
                                     n_repeats=iterations,
                                     random_state=random_state)
    elif Type == "StratifiedShuffleSplit":
        kf = StratifiedShuffleSplit(n_splits=n_splits, 
                                    test_size=test_size, 
                                    random_state=random_state)
    elif Type == "PredefinedSplit":
        kf = PredefinedSplit(test_fold=test_fold)
    else:
        print('No valid crossvalidation approach specified.')
        return

    if max(Diagnosis_standardized) == 1:
        scoring = 'roc_auc'
    else:
        # Hand and Till multiclass AUC
        multi_class_auc_scorer = make_scorer(multi_class_auc, needs_proba=True)
        scoring = multi_class_auc_scorer

    y_test_iteration = []
    y_score_iteration = []
    y_pred_iteration = []
    
    train_ids = []
    test_ids = []

    count = 0
    for train_index, test_index in kf.split(Data_standardized, Diagnosis_standardized):
            
        count = count + 1

        if count <= count_previous:
            continue
        print(count, ' / ', n_splits*iterations)
        sys.stdout.flush()
        
        train_indices.append(train_index.tolist())
        test_indices.append(test_index.tolist())
        
        train_ids.append(SubjectID[train_index].tolist())
        test_ids.append(SubjectID[test_index].tolist())

        if kernel == 'precomputed':
            X_train = Data_standardized[np.ix_(train_index, train_index)]
            y_train = Diagnosis_standardized[train_index]
            X_test = Data_standardized[np.ix_(test_index, train_index)]
            y_test = Diagnosis_standardized[test_index]  
        else:
            X_train = Data_standardized[train_index]
            y_train = Diagnosis_standardized[train_index]
            X_test = Data_standardized[test_index]
            y_test = Diagnosis_standardized[test_index]

        # Optional: SMOTE to correct inbalanced classes

        # Optional: feature selection


        if UseSavedModel: 
            # load the model from disk
            clf = pickle.load(open(UseSavedModel, 'rb'))
        else:
            # Train the model
            
            # Type of kernel
            # To do: investigate if balanced class weight changes AD CN results
            classifier = SVC(kernel=kernel, probability=True, random_state=0, class_weight = 'balanced')
    
            # Train classifier
            clf, best_c_value, best_estimator = gridsearch(
                                                    X_train, y_train,
                                                    classifier=classifier,
                                                    scoring=scoring,
                                                    nFeatures=nFeatures)
            clf, best_c_value, best_estimator = gridsearch(
                                                    X_train, y_train,
                                                    classifier=classifier,
                                                    coarse_c_value=best_c_value,
                                                    scoring=scoring)
    
            clfs.append(clf)
            best_c_values.append(best_c_value)

        # save the model to disk
        pickle.dump(clf, open(model_filename, 'wb'))
            
        # Apply classifier to test set (probabilistic and binary output)
        y_score = clf.predict_proba(X_test)
        y_pred = clf.predict(X_test)

        y_test_iteration.extend(y_test)
        y_score_iteration.extend(list(y_score))
        y_pred_iteration.extend(y_pred)

        # Compute metrics 
        if Type == "RepeatedStratifiedKFold" and np.mod(count, n_splits):
            # For RepeatedStratifiedKFold metrics are only computed after k interations
            continue
        else:
            # Compute confusion matrix
            conf = confusion_matrix(y_test_iteration, y_pred_iteration).tolist()
            
            print(conf)

            # Compute Accuracy
            accuracy = accuracy_score(y_test_iteration, y_pred_iteration)
            accs.append(accuracy)  # fill array with accuracies
            
            print(y_test_iteration)
            print(accuracy)

            # Compute area under the ROC Curve (AUC)
            if len(np.unique(y_test_iteration)) <= 2:
                auc = roc_auc_score(y_test_iteration, np.array(y_score_iteration)[:,1])    
            else:
                auc = multi_class_auc(y_test_iteration, np.array(y_score_iteration))
                #  from functions.MAUC import MAUC
                #  auc2= MAUC(list(zip(y_test, y_score)), max(Diagnosis_standardized)+1) #TADPOLE implementation
            print(auc)
            aucs.append(auc)  # fill array with AUCs

            if SaveTempFile:
                print('Temporary file saved: ', TempFile)
                with open(TempFile, 'wb') as TF:
                    pickle.dump((count,
                                iterations * n_splits,
                                Data_standardized,
                                Diagnosis_standardized,
                                Labels,
                                iterations,
                                n_splits,
                                kernel,
                                SaveTempFile,
                                TempFileLocation,
                                clfs,
                                conf,
                                best_c_values,
                                accs,
                                aucs,
                                train_indices,
                                test_indices,
                                y_test_iteration,
                                y_score_iteration,
                                y_pred_iteration), TF)

            y_test_iteration = []
            y_score_iteration = []
            y_pred_iteration = []
           

#    # Clean-up temp file
#    if os.path.exists(TempFile):
#        os.remove(TempFile)
        
    # Confidence interval computation using corrected resampled t-test (Nadeau and Bengio, Machine Learning, 2003)
    N_1 = float(len(train_index))
    N_2 = float(len(test_index)) 
    print( f"Train size: {N_1}, Test size: {N_2}")
    alpha=0.95 

    p = {}
    p['conf'], p['best_c_values'], p['accs'], p['aucs'] = conf, best_c_values, accs, aucs

    # Compute mean performance
    p['c_mean'] = np.mean(p['best_c_values'])
    p['c_std'] = np.std(p['best_c_values'])
    p['acc_mean'] = np.mean(p['accs'])
    p['acc_std'] = np.std(p['accs'])
    p['acc_95ci'] = compute_CI.compute_confidence(p['accs'], N_1, N_2, alpha)
    
    p['auc_mean'] = np.mean(p['aucs'])
    p['auc_std'] = np.std(p['aucs'])
    p['auc_95ci'] = compute_CI.compute_confidence(p['aucs'], N_1, N_2, alpha)
    
    if Type == "StratifiedShuffleSplit":
        print('mean C:', '%.3f' % p['c_mean'], ' (',  '%.3f' % p['c_std'], ')')
        print('mean Acc:', '%.3f' % p['acc_mean'], ' (',  '%.3f' % p['acc_95ci'][0], ' - ', '%.3f' % p['acc_95ci'][1], ')')
        print('mean AUC:', '%.3f' % p['auc_mean'], ' (',  '%.3f' % p['auc_95ci'][0], ' - ', '%.3f' % p['auc_95ci'][1], ')') 
    else:
        print('mean C:', '%.3f' % p['c_mean'], ' (',  '%.3f' % p['c_std'], ')')
        print('mean Acc:', '%.3f' % p['acc_mean'], ' (',  '%.3f' % p['acc_std'], ')')
        print('mean AUC:', '%.3f' % p['auc_mean'], ' (',  '%.3f' % p['auc_std'], ')')

    p['computation_time[s]'] = round(time.time() - t)
    
    ids = {}
    ids['train'] = train_ids
    ids['test'] = test_ids
    
    return p, ids


def gridsearch(X_train,
               y_train,
               classifier=SVC(kernel='linear'),
               coarse_c_value=0,
               folds_gridsearch=5,
               scoring='roc_auc',
               nFeatures = 1):

    # Define range for the C value Gridsearch (logaritic range recommented)
    if coarse_c_value:
        # Fine range
        C_range = np.logspace((np.log2(coarse_c_value))-1,
                              (np.log2(coarse_c_value))+1, 15, base=2)
    else:
        # Coarse range
        # C_range = np.logspace(-5, 10, 16, base=2)
        C_range = np.logspace(-5, 5, 11, base=2)

    # Define grid for parameters
    param_grid = {'C': C_range}

    # Define classifier, cross-validated gridsearch
    clf = GridSearchCV(classifier,
                       param_grid,
                       cv=folds_gridsearch,
                       scoring=scoring)
    clf.fit(X_train, y_train)

    best_c_value = clf.best_params_['C']
    best_estimator = clf.best_estimator_

    return clf, best_c_value, best_estimator
