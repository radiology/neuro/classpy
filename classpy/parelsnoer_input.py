import numpy as np
import pandas as pd
#from datetime import date, timedelta 
      
def prepare_data(df, minimum_followup_months=6, write_csv=0):

    # Set PTID
    df = df.rename(columns={'MRI_ID_XNAT': 'PTID'})

    # Set variable names confounders
    df = df.rename(columns={'GESLACHT': 'Sex'})
    df['Sex'] = df['Sex'].map({'Man': 'M', 'Vrouw': 'F'})
    df = df.rename(columns={'SYNDR_DIAG.1': 'Diagnosis'})

    # Set Baseline diagnosis
    df['Diagnosis'] = df['Diagnosis'].map({'subjectieve cognitieve klachten': 'CN', 'MCI': 'MCI', 'dementie': 'AD'})

    # Compute Age
    
    # To make the origins of SPSS and Python match each other is necessary to rescale the integer from SPSS with the number 12219379200 i.e. the number of seconds existing between "1582-10-14" and "1970-01-01" (the origin used by to_datetime)
    time_origin = 12219379200
    time_origin_array = np.array([time_origin for i in range(0,len(df['MRI_Date_XNAT']))])
    df['MRI_Date_XNAT'] = pd.to_datetime(np.subtract(df['MRI_Date_XNAT'].to_numpy(), time_origin_array), unit="s")
    df['GEB_DAT'] = pd.to_datetime(df['GEB_DAT'], format='%Y-%m-%d')
    
    
    df['Age'] = df.apply(
               lambda x: x['MRI_Date_XNAT'].year - x['GEB_DAT'].year - 
               ((x['MRI_Date_XNAT'].month, x['MRI_Date_XNAT'].day) < (x['GEB_DAT'].month, x['GEB_DAT'].day)),
               axis=1)

        
    ## Find MCI converters
    # Compute last diagnosis
    condlist = [df['SYNDR_DIAG.7'].notna(), df['SYNDR_DIAG.6'].notna(), df['SYNDR_DIAG.5'].notna(), df['SYNDR_DIAG.4'].notna(), df['SYNDR_DIAG.3'].notna(), df['SYNDR_DIAG.2'].notna()]
    choicelist = [df['SYNDR_DIAG.7'], df['SYNDR_DIAG.6'], df['SYNDR_DIAG.5'], df['SYNDR_DIAG.4'], df['SYNDR_DIAG.3'], df['SYNDR_DIAG.2']]
    df['SYNDR_DIAG_last'] = np.select(condlist, choicelist, default=df['Diagnosis'])
    df['SYNDR_DIAG_last'] = df['SYNDR_DIAG_last'].map({'subjectieve cognitieve klachten': 'CN', 'CN': 'CN', 'MCI': 'MCI', 'dementie': 'AD', 'AD': 'AD'})

    # Compute followup time
    for i in range(1,8):
        df['VISITE_DAT.' + str(i)] = pd.to_datetime(np.subtract(df['VISITE_DAT.' + str(i)].to_numpy(), time_origin_array), unit="s")
   
    condlist = [df['VISITE_DAT.7'].notna(), df['VISITE_DAT.6'].notna(), df['VISITE_DAT.5'].notna(), df['VISITE_DAT.4'].notna(), df['VISITE_DAT.3'].notna(), df['VISITE_DAT.2'].notna()]
    choicelist = [df['VISITE_DAT.7'], df['VISITE_DAT.6'], df['VISITE_DAT.5'], df['VISITE_DAT.4'], df['VISITE_DAT.3'], df['VISITE_DAT.2']]
    df['VISITE_DAT_last'] = np.select(condlist, choicelist, default=df['VISITE_DAT.1'])
    
    df['followup_time_years'] = [i.days/365.25 for i in (df['VISITE_DAT_last'] - df['MRI_Date_XNAT'])]

    # Find MCI converters 
    df.loc[df.Diagnosis.isin(['MCI']) &  df.SYNDR_DIAG_last.isin(['AD']) & (df.followup_time_years > minimum_followup_months / 12), 'MCI converter (' + str(minimum_followup_months) + ')'] = 1 
    df.loc[df.Diagnosis.isin(['MCI']) & ~df.SYNDR_DIAG_last.isin(['AD']) & (df.followup_time_years > minimum_followup_months / 12), 'MCI converter (' + str(minimum_followup_months) + ')'] = 0

    # Find MCI converters 
    df.loc[df.Diagnosis.isin(['MCI']) &  df.SYNDR_DIAG_last.isin(['AD']) & (df.followup_time_years > minimum_followup_months / 12), 'Diagnosis MCI'] = 'MCIc' 
    df.loc[df.Diagnosis.isin(['MCI']) & ~df.SYNDR_DIAG_last.isin(['AD']) & (df.followup_time_years > minimum_followup_months / 12), 'Diagnosis MCI'] = 'MCI' 

    
    # Print some demographics
    print('\nSizes diagnostic groups at baseline:')
    print(df['Diagnosis'].value_counts())
    
    diag_list = [i for i in list(set(df['Diagnosis'].tolist()))]
    print('\nSizes diagnostic groups at baseline that have >0.5y followup:')
    temp = df[(df['followup_time_years'] > 0.5)].copy()
    print(temp['Diagnosis'].value_counts())
    
    print('\nFollow-up diagnoses for each baseline diagnosis:')
    for i in diag_list:
        print('\n' + i)
        temp = df[(df['followup_time_years'] > 0.5) & (df['Diagnosis'].isin([i]))].copy()
        print(temp['SYNDR_DIAG_last'].value_counts())

    print(df['MCI converter (' + str(minimum_followup_months) + ')'].value_counts())

    # Clean-up file
    keepColumns = ['PTID', 'Sex', 'Diagnosis', 'Age', 'MCI converter (' + str(minimum_followup_months) + ')', 'Diagnosis MCI']
    removeColumns = [ i for i in df.keys() if i not in keepColumns]
    df = df.drop(removeColumns, axis=1)

    return df