import os
import pandas as pd
import numpy as np
import h5py

def pdReadImageData(directory_list, pdData):

    print(pdData['Diagnosis']) 
    for hdf5_filename in set(pdData['hdf5'].values):
        for directory in directory_list:
            hdf5_filename_dir = os.path.join(directory, hdf5_filename)
            if not os.path.exists(hdf5_filename_dir):
                continue
            with h5py.File(hdf5_filename_dir, 'r') as f:
                features = np.array(f['data'])    
                pid=[i.decode("utf-8")  for i in list(f['PTID'])]
                
                if 'pdFeatures' not in locals():
                    pdFeatures = pdImageDataIntoDataFrame(features, pid)
                else:
                    pdFeatures_temp = pdImageDataIntoDataFrame(features, pid)
                    pdFeatures = pdFeatures.append(pdFeatures_temp)   
                    del pdFeatures_temp
                del features            
        
    if len(directory_list)==1:    
        pdData = pdData.merge(pdFeatures, how='inner', on='PTID')
    else:
        pdData = pdData.merge(pdFeatures, how='outer', on='PTID')
    pdData = pdData.drop(labels=['images','hdf5'],axis=1)
        
    print('Dataset loaded with ' + str(pdData.shape[1]) + ' features and ' + str(pdData.shape[0]) + ' samples.' )
    print(pdData['Diagnosis'])    
    return pdData


def pdImageDataIntoDataFrame(features, pid):
    column_names = ['voxel_' + str(i) for i in range(0,features.shape[1])]
    pdFeatures = pd.DataFrame(data=features, columns=column_names)
    pdFeatures['PTID'] = pd.Series(pid)
    
    return pdFeatures
