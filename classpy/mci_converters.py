'''
Write CSV file with MCI converters
'''

import classpy.tadpole_input as ti
import pandas as pd


str_data_element = 'C://Users//Esther//Documents//Project//ADNI//TADPOLE_D1_D2_demographics.csv'
Data = pd.read_csv(str_data_element)
Data_12 = ti.prepare_data(Data.copy(), maximum_followup_months=12, write_csv=1)

Data_temp = ti.prepare_data(Data.copy(), maximum_followup_months=24, write_csv=1)
Data_12 = pd.concat([Data_12, Data_temp['MCI converter (24)']], axis=1, join_axes=[Data_12.index])

Data_temp = ti.prepare_data(Data.copy(), maximum_followup_months=36, write_csv=1)
Data_12 = pd.concat([Data_12, Data_temp['MCI converter (36)']], axis=1, join_axes=[Data_12.index])

Data_bl = Data_12[Data_12['ID'].str.contains("_bl")]
removeColumns = ['ID', 'Age', 'Sex', 'ICV']
Data_csv = Data_bl.drop(removeColumns, axis=1)

Data_csv.to_csv('C://Users//Esther//Documents//Project//ADNI//MCI_converters_feb2019.csv')