from classpy import read_configuration as rc
from classpy import prepare_inputs as pi
from classpy import prepare_image_inputs as pii
from classpy.crossvalidation import crossvalidation as cv
from classpy import save_results as sr
import os
import numpy as np
import pickle
import datetime
import errno

"""ClassPy - A python toolbox for classification

do_classification is the main function.

Esther Bron - e.bron@erasmusmc.nl
"""

print('Start classification...') 

config_file = './resources/adni_vox_cluster.ini'

# Read configuration file
if os.path.exists(config_file):
    p = rc.read_config(config_file)
else:    
    raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), config_file)
del config_file    
    
print(p)

if not os.path.exists(p['ResultsFolder']):
    os.mkdir(p['ResultsFolder'])

# Read data
pdData_all, UniqueSubjIDs = pi.pdReadData(p['DataIn'],
                                          0,
                                          Labels=p['Labels'])

pdDataTest_all, UniqueTestSubjIDs = pi.pdReadData(p['DataTest'],
                                                  0,
                                                  Labels=p['Labels'])
#del UniqueSubjIDs
#del UniqueTestSubjIDs

# Read voxelwise feature from image
if 'hdf5' in pdData_all:
    pdData_all_temp = pdData_all.copy()
    del pdData_all
    pdData_all = pii.pdReadImageData(p['DataInDirectory'], pdData_all_temp)

if 'hdf5' in pdDataTest_all:
    pdDataTest_all_temp = pdDataTest_all.copy()
    del pdDataTest_all    
    pdDataTest_all = pii.pdReadImageData(p['DataTestDirectory'], pdDataTest_all_temp)

pdData_all, pdDataTest_all, BiomarkersList, GroupValues = pi.CorrectConfounders(pdData_all, pdDataTest_all, Factors=p['Factors'])
del GroupValues

# Remove unwanted biomarkers
BiomarkersList_all = [b for b in BiomarkersList if b not in p['Biomarkers_exclude']]
del BiomarkersList

Data_all = pi.pd2mat(pdData_all, BiomarkersList_all, 0)
Diagnosis = pdData_all['Diagnosis'].values
print(Diagnosis)
SubjectID = pdData_all['PTID'].values
#del pdData_all
del pdDataTest_all
del BiomarkersList_all

# Select timepoints
p['Timepoints'] = 0
Data_baseline = Data_all[:, :, p['Timepoints']]
del Data_all

# Handling missing data (remove incomplete sets)
idx_complete = np.squeeze(np.isnan(np.sum(Data_baseline, 1))) == 0
idx_complete = np.squeeze(np.where(idx_complete))
Data_complete = Data_baseline[idx_complete, :]
Diagnosis_complete = Diagnosis[idx_complete]
SubjectID_complete = SubjectID[idx_complete]
del Diagnosis
del Data_baseline
del SubjectID
del idx_complete

print(Diagnosis_complete)

# Cross-validaton
p_cv, ids = cv(Data_complete, Diagnosis_complete, SubjectID=SubjectID_complete, Labels=p['Labels'], Type=p['Type'], iterations=p['Iterations'], n_splits=p['N_splits'], test_size=p['test_size'], kernel=p['Kernel'], TempFileLocation=p['TempDirectory'], InputTempFile=p['InputTempFile'], TestClass=p['TestClass'], UseSavedModel=p['UseSavedModel'], ResultsFolder=p['ResultsFolder'] )

p.update(p_cv)

# Save Results
if p['SaveResults']:  
    p['nFeatures'] = Data_complete.shape[1]
    p['nTrain'] = Data_complete.shape[0]
    p['nClasses'] = np.unique(Diagnosis_complete).shape[0]
    sr.save_results_as_csv(p, p['ResultsFile'])
    sr.save_results_as_csv(p, os.path.join(p['ResultsFolder'], 'results.csv'))

    p['ResultsDump'] = os.path.join(p['ResultsFolder'], 'results.pickle')
    
    # Add train and test ids   
    p.update(ids)
    p['SubjectID'] = SubjectID_complete
    with open(p['ResultsDump'], 'wb') as RF:
        pickle.dump(p,RF)   
    
 

