import pandas as pd
import numpy as np
import os
import nibabel as nib
import h5py
import classpy.tadpole_input as ti

# Set directory
directory = '/scratch/ebron/ADNI'
if not os.path.exists(directory):
    directory = 'C:/Users/Esther/Documents/Project/ADNI'
if not os.path.exists(directory):
    directory = '/projects/0/emc16367/ADNI/'

# Input files
demographics_file = os.path.join(directory, 'TADPOLE_D1_D2_demographics.csv')
image_directory = os.path.join(directory, 'Template_space')
image_file = os.path.join('Brain_image_in_MNI_space', 'result.nii.gz')
mask_filename = os.path.join(directory, 'Template_space', 'brain_mask_in_template_space.nii.gz')
#mask_filename = os.path.join(directory, 'Template_space', 'hippocampus_in_template_space.nii.gz')
#mask_filename = os.path.join(directory, 'Template_space', 'gm_in_template_space.nii.gz')
data_description = 'AD ADNI set, affinely registered T1 images, masked by whole brain'
diagnosis_list = ['AD', 'CN']

# Output files
output_filename = 'adni_ad_t1_baseline_masked'
#output_filename = 'adni_all_baseline_hippocampus'
#output_filename = 'adni_test_template_masked'
output_csv = os.path.join(directory, output_filename + '.csv')
output_hdf5 = os.path.join(directory, output_filename + '.hdf5')

## Start doing things

# Read ids from demographics file
df = pd.read_csv(demographics_file)
if 'TADPOLE' in demographics_file:
    df = ti.prepare_data(df)
    df = df.drop('PTID',axis=1)
    df = df.rename(columns={'ID':'PTID'})

# Find existing images and make new dataframe
timepoints = ['_bl']
image_locations = [os.path.join(image_directory, s, image_file) if any(t in s for t in timepoints) and d in diagnosis_list else '' for s,d in zip(df['PTID'],df['Diagnosis'])]

existing_image_locations = [a if os.path.exists(a) else np.nan for a in image_locations ]
df['images'] = pd.Series(existing_image_locations, index=df.index)

#df_bl = df.loc[df['VISCODE'] == 'bl']
df_output = df.loc[ pd.notna(df['images']) ]

# Save csv output (Input for ClassPy)
df_output['hdf5'] = [output_filename + '.hdf5' for i in df_output['PTID'].tolist()]
df_output.to_csv(output_csv, index=False)

# Start reading images
# Read mask file, save as 1d vector per subject.
if os.path.exists(mask_filename):
    mask_nifti = nib.load(mask_filename)
    mask_image = mask_nifti.get_data()
    mask_shape=mask_image.shape
    mask_1d = mask_image.flatten(order='C')
#   # Code for recovering mask
#    mask_image_recovered=np.reshape(mask_1d,mask_shape)


# Open output hdf5 file (Input for Classpy)
with h5py.File(output_hdf5, 'w') as f:
    # Loop over all existing subject images
    for i, (index, filename) in enumerate(df_output['images'].iteritems()):
#        if i % 2:
#            continue

        # Read image file, save as 1d vector 
        nifti = nib.load(filename)
        image = nifti.get_data()
        image_1d = image.flatten(order='C')
        
#        # Mask 3D image (commented out, just for later review)     
#        image_masked = image.copy()
#        image_masked [np.where(np.logical_not(mask_image))] = 0
        
        # Apply mask to image
        if os.path.exists(mask_filename):
            image_1d_masked = image_1d[np.where(mask_1d)]
            
#            # Code for recovering image (commented out, just for later review) 
#            image_1d_recovered=mask_1d.astype(np.float32,order='C')
#            image_1d_recovered[np.where(mask_1d)]=image_1d_masked
#            image_recovered=np.reshape(image_1d_recovered,mask_shape)
#            print(np.min(np.subtract(image_masked,image_recovered)))

            image_1d = image_1d_masked
            del image_1d_masked
        
        # Create dataset in the first iteration
        if i == 0 : 
            n_items=len(df_output['images'])
            data_shape = (n_items,) + image_1d.shape
            dataset = f.create_dataset('data', data_shape, 'f', compression="gzip")
            ptid = f.create_dataset('PTID', data=np.array([''] * n_items, dtype='S32'))
        
        print(i)
        # Store masked 1D image in dataset
        dataset[i,:] = image_1d
        
        # Store patient ID
        ptid[i] = df_output['PTID'][index].encode("utf-8")
    
    
    print(list(ptid))
    # Save attributes
    print('Saving attributes...')
    dataset.attrs['description'] = data_description
    dataset.attrs['demographics_file'] = np.array(demographics_file, dtype='S')
    dataset.attrs['nSamples'] = n_items
    dataset.attrs['nFeatures'] = image_1d.shape[0]
    if os.path.exists(mask_filename):
        dataset.attrs['mask'] = mask_filename
    for j, label in enumerate(['samples', 'features']):
        dataset.dims[j].label = label  
    
    print('Printing dataset..')
    print(list(dataset))
    print(list(dataset.attrs.items()))
    print('Saving dataset..')
    
